package com.example.testenvelope;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.hellolibs.HelloLibs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createHasplm();

        HelloLibs helloLibs = new HelloLibs();

        TextView tv = new TextView(this);
        tv.setText(helloLibs.stringFromJNI());
        setContentView(tv);
    }

    private void createHasplm() {
        String hasplmDir = "/.hasplm";
        String iniFile = hasplmDir + "/hasp_107506.ini";

        String iniFileData = "serveraddr=ec2-18-159-132-134.eu-central-1.compute.amazonaws.com\n" +
                "broadcastsearch=0\n" +
                "requestlog=1\n" +
                "errorlog=1";

        createSubdir(hasplmDir);
        writeToFile(iniFile, iniFileData);

        Log.i("SavedData", "Try read from file: " + iniFile);
        String savedData = readFromFile(iniFile);
        Log.i("SavedData", savedData);
    }

    private void createSubdir(String dirName) {
        String dirPath = getFilesDir().getAbsolutePath() + dirName;
        File projDir = new File(dirPath);
        if (!projDir.exists())
            projDir.mkdirs();
    }

    private void writeToFile(String iniFileName, String data) {
        try {
            String filesPath = getFilesDir().getAbsolutePath();
            File iniFile = new File(filesPath, iniFileName);

            FileOutputStream stream = new FileOutputStream(iniFile);
            stream.write(data.getBytes());
            stream.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private String readFromFile(String iniFile) {
        String iniFilePath = getFilesDir().getAbsolutePath() + iniFile;

        String ret = "";
        try {
            InputStreamReader inputStreamReader = new FileReader(new File(iniFilePath));
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append("\n").append(receiveString);
            }

            ret = stringBuilder.toString();
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
}